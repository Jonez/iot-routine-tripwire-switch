void activeSensorOut(int modeNum, int sensorNum){
  switch (modeNum) {
    case 3:
      wifiSet();
      detected = true;
      webEvent = zenOut1;
      get_http();
      registerBuzzer();
      sadFaceDisplay();
      break;
    
    case 4:
      wifiSet();
      detected = true;
      webEvent = zenOut2;
      get_http();
      registerBuzzer();
      sadFaceDisplay();
      break;
      
    case 7:
      wifiSet();
      detected = true;
      webEvent = zenOut3;
      get_http();
      registerBuzzer();
      sadFaceDisplay();
      break;
      
    case 9:
      wifiSet();
      detected = true;
      if(toggletm1 == true){
        webEvent = zenOut1;
      }
      if(toggletm2 == true){
        webEvent = zenOut2;
      }
      if(toggletm3 == true){
        webEvent = zenOut3;
      }
      get_http();
      registerBuzzer();
      sadFaceDisplay();
      break;
      
    case 11:
      detected = true;
      registerBuzzer();
      if(sensorNum == 1){
        sensor1 = 1;
        sensor2 = 0;
        setSensor1(1);
        setSensor2(0);
      }
      if(sensorNum == 2){
        sensor1 = 0;
        sensor2 = 1;
        setSensor1(0);
        setSensor2(1);
      }
      setupProcess();
      break;
  }
}
