
int freq = 2000;
int channel = 0;
int resolution = 10;

void buzzerSet(){
  pinMode(buzzer, OUTPUT);
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(buzzer, channel);
  pinMode(buzzer, INPUT);
}

void registerBuzzer(){
  if(buzzerON == "true"){
    pinMode(buzzer, OUTPUT);
    ledcWrite(channel, 125);
    delay(150);
    ledcWrite(channel, 0);
    pinMode(buzzer, INPUT);
  }
}
