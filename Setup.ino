void setup() 
{
  Serial.begin(115200);
  setCpuFrequencyMhz(80);
  Wire.begin();
  displaySet();
  batterySetup();
  sdSetup();
  AllData();
  rangeSet();
  buzzerSet();
  Serial.println(rangeDistance);
  wifiSet();
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();
}
