
void rangeSet() {    
  tcaselect(0);
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    displayError();
    while (1);
  }

  delay(100);
  
  tcaselect(1);
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    displayError();
    while (1);
  }
}

void rangeRead() {
    VL53L0X_RangingMeasurementData_t measure;
    tcaselect(0);
    lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
    
    int milimeter = measure.RangeMilliMeter;
    unsigned long currentMillis = millis();
    falseBuzz = false;
   
    if(milimeter <= rangeDistance){   
      setCpuFrequencyMhz(80);
      if (currentMillis - previousMillis >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;
        if(measure.RangeMilliMeter <= rangeDistance){
          tick++;
        }      
      }  
      pastSelected = false;
          // will run when tick equals 5 seconds
          if (tick >= 5) {
            detected = true;
            motion = false;
            selectedMode = true;
            if (milimeter >= 1 && milimeter < 100) {
              mode1();
              modeNum = 2;
              startSetup = false;
            }
            if (milimeter >= 100 && milimeter < 150) {
              mode2();
              modeNum = 4;
              startSetup = false;
            }
            if (milimeter >= 150 && milimeter < 200) {
              mode3();
              modeNum = 6;
              startSetup = false;
            }
            if (milimeter >= 200 && milimeter < 250) {
              timeMode();
              modeNum = 8;
              startSetup = false;
            }
            if (milimeter >= 250 && milimeter <= 300) {
              modeSetup();
              modeNum = 10;
              startSetup = true;
            }
            if(milimeter > 300){
              falseBuzz = true;
            }
            countFiveTicks();
        }
        modeSelected(modeNum);
    } 
    else {
          tick = 0;
          tempCheck = 0;
          selectedMode = false;
    }

    unsigned long motionMillis = 0;
    
    tcaselect(0);
    lox.rangingTest(&measure, false); 
    
    if(tick < 5){
      int sensor1Num = 1;
      if(measure.RangeMilliMeter <= rangeDistance){
        setCpuFrequencyMhz(80);
        detected = true;
        motion = true;
        sensorIn = true;
      }
      if(pastSelected == false){
        if(motion == true && selectedMode == false && sensorIn == true){
          if(currentMillis - motionMillis >= interval){
            if(tick == 0 && milimeter > rangeDistance){
              if(sensor1 == 0 && sensor2 == 1){
                if(startSetup == false){
                  if(pCount > 0){
                    pCount--;
                    straightFaceDisplay();
                    delay(2000);
                  }
                  Serial.println(pCount);
                  if(pCount == 0){
                    activeSensorOut(modeNum+1, sensor1Num);
                  }
                }
                if(startSetup == true){
                  Serial.println("triggered");
                  activeSensorOut(modeNum+1, sensor1Num);
                }
              }else{
                if(startSetup == false){
                  pCount++;
                  if(pCount != 1){
                    straightFaceDisplay();
                  }
                  Serial.println(pCount);
                  if(pCount == 1){
                    activeSensorIn(modeNum, sensor1Num);
                  }
                }
                if(startSetup == true){
                  Serial.println("triggered");
                  activeSensorIn(modeNum, sensor1Num);
                }
              }
              if(pCount > 1){
                delay(2000);
              }
              sensorIn = false;
              motion = false;
            }
          }
          motionMillis = currentMillis;
        }
      }
    }

    tcaselect(1);
    lox.rangingTest(&measure, false);
     
    if(tick < 5){
      int sensor2Num = 2;
      if(measure.RangeMilliMeter <= rangeDistance){
        setCpuFrequencyMhz(80);
        detected = true;
        motion = true;
        sensorIn = true;
      }
      if(pastSelected == false){
        if(motion == true && selectedMode == false && sensorIn == true){
          if(currentMillis - motionMillis >= interval){
            if(tick == 0 && milimeter > rangeDistance){
              if(sensor1 == 1 && sensor2 == 0){
                if(startSetup == false){
                  if(pCount > 0){
                    pCount--;
                    straightFaceDisplay();
                    delay(2000);
                  }
                  Serial.println(pCount);
                  if(pCount == 0){
                    activeSensorOut(modeNum+1, sensor2Num);
                  }
                }
                if(startSetup == true){
                  Serial.println("triggered");
                  activeSensorOut(modeNum+1, sensor2Num);
                }
              }else{
                if(startSetup == false){
                  pCount++;
                  if(pCount != 1){
                    straightFaceDisplay();
                  }
                  Serial.println(pCount);
                  if(pCount == 1){
                    activeSensorIn(modeNum, sensor2Num);
                  }
                }
                if(startSetup == true){
                  Serial.println("triggered");
                  activeSensorIn(modeNum, sensor2Num);
                }
              }
              if(pCount > 1){
                delay(2000);
              }
              sensorIn = false;
              motion = false;
            }
          }
          motionMillis = currentMillis;
        }
      }
    }
}

void countFiveTicks(){
  if(tick == 5){
    tempCheck = 10;
  }
}

void modeSelected(int modeNum){
  unsigned long selectedMillis = 0;
  //Counts Ticks To Change Mode 
  if(tick == 10){
    registerBuzzer();
    if(startSetup == true){
      selectInSensorDisplay();
      rangeDistance = 100;
    }else{
      setSelectedMode(modeNum);
      motion = false;
      tick = 0;
      pastSelected = true;
    }
    selectedMode = true;
    delay(1000);
  }
}
