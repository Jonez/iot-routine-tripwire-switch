#include <SPI.h>
#include <SD.h>

const int chipSelect = 5;

String storeWifi;
String storePassword;
String storeIFTTTWebkey;
String storeBuzzerOn;
String storeEventIn1;
String storeEventIn2;
String storeEventIn3;
String storeEventOut1;
String storeEventOut2;
String storeEventOut3;
int storeSelectedMode;
int storeRange;
int storeSensor1;
int storeSensor2;
String storeTimeOff;
String storeTimeOn;
String storeTimeMode1;
String storeTimeMode2;
String storeTimeMode3;
long storeGmtOffset_sec;
int storeDaylightOffset_sec;

File myFile;

void sdSetup(){
   while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  Serial.println("Initializing SD card...");
  pinMode(chipSelect, OUTPUT);
  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    displayError();
    return;
  }
  Serial.println("initialization done.");

  myFile = SD.open("/Variables/ssid.txt");
  if (myFile) {
    String wifi;
    while (myFile.available()) {
      char c = myFile.read();
      wifi.concat(c);
    }
    storeWifi = wifi;
    myFile.close();
  }

  myFile = SD.open("/Variables/password.txt");
  if (myFile) {
    String pwd;
    while (myFile.available()) {
      char c = myFile.read();
      pwd.concat(c);
    }
    storePassword = pwd;
    myFile.close();
  } 

  myFile = SD.open("/Variables/IFTTTWebkey.txt");
  if (myFile) {
    String key;
    while (myFile.available()) {
      char c = myFile.read();
      key.concat(c);
    }
    storeIFTTTWebkey = key;
    myFile.close();
  } 

  myFile = SD.open("/Variables/buzzerOn.txt");
  if (myFile) {
    String buzz;
    while (myFile.available()) {
      char c = myFile.read();
      buzz.concat(c);
    }
    storeBuzzerOn = buzz;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventIn1.txt");
  if (myFile) {
    String in1;
    while (myFile.available()) {
      char c = myFile.read();
      in1.concat(c);
    }
    storeEventIn1 = in1;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventIn2.txt");
  if (myFile) {
    String in2;
    while (myFile.available()) {
      char c = myFile.read();
      in2.concat(c);
    }
    storeEventIn2 = in2;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventIn3.txt");
  if (myFile) {
    String in3;
    while (myFile.available()) {
      char c = myFile.read();
      in3.concat(c);
    }
    storeEventIn3 = in3;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventOut1.txt");
  if (myFile) {
    String out1;
    while (myFile.available()) {
      char c = myFile.read();
      out1.concat(c);
    }
    storeEventOut1 = out1;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventOut2.txt");
  if (myFile) {
    String out2;
    while (myFile.available()) {
      char c = myFile.read();
      out2.concat(c);
    }
    storeEventOut2 = out2;
    myFile.close();
  } 

  myFile = SD.open("/Variables/eventOut3.txt");
  if (myFile) {
    String out3;
    while (myFile.available()) {
      char c = myFile.read();
      out3.concat(c);
    }
    storeEventOut3 = out3;
    myFile.close();
  } 

  myFile = SD.open("/Variables/timeOff.txt");
  if (myFile) {
    String toff;
    while (myFile.available()) {
      char c = myFile.read();
      toff.concat(c);
    }
    storeTimeOff = toff;
    myFile.close();
  } 

  myFile = SD.open("/Variables/timeOn.txt");
  if (myFile) {
    String ton;
    while (myFile.available()) {
      char c = myFile.read();
      ton.concat(c);
    }
    storeTimeOn = ton;
    myFile.close();
  } 

  myFile = SD.open("/Variables/timeMode1.txt");
  if (myFile) {
    String tmod1;
    while (myFile.available()) {
      char c = myFile.read();
      tmod1.concat(c);
    }
    storeTimeMode1 = tmod1;
    myFile.close();
  } 

  myFile = SD.open("/Variables/timeMode2.txt");
  if (myFile) {
    String tmod2;
    while (myFile.available()) {
      char c = myFile.read();
      tmod2.concat(c);
    }
    storeTimeMode2 = tmod2;
    myFile.close();
  } 

  myFile = SD.open("/Variables/timeMode3.txt");
  if (myFile) {
    String tmod3;
    while (myFile.available()) {
      char c = myFile.read();
      tmod3.concat(c);
    }
    storeTimeMode3 = tmod3;
    myFile.close();
  } 

  myFile = SD.open("/Variables/gmtOffset_sec.txt");
  if (myFile) {
    String gtmoff;
    while (myFile.available()) {
      char c = myFile.read();
      gtmoff.concat(c);
    }
    storeGmtOffset_sec = gtmoff.toInt();
    myFile.close();
  } 

  myFile = SD.open("/Variables/daylightOffset_sec.txt");
  if (myFile) {
    String daylight;
    while (myFile.available()) {
      char c = myFile.read();
      daylight.concat(c);
    }
    storeDaylightOffset_sec = daylight.toInt();
    myFile.close();
  } 

  myFile = SD.open("/AutoSetup/selectedMode.txt");
  if (myFile) {
    String sm;
    while (myFile.available()) {
      char c = myFile.read();
      sm.concat(c);
    }
    storeSelectedMode = sm.toInt();
    myFile.close();
  } 

  myFile = SD.open("/AutoSetup/range.txt");
  if (myFile) {
    String rng;
    while (myFile.available()) {
      char c = myFile.read();
      rng.concat(c);
    }
    storeRange = rng.toInt();
    myFile.close();
  } 

  myFile = SD.open("/AutoSetup/sensor1.txt");
  if (myFile) {
    String s1;
    while (myFile.available()) {
      char c = myFile.read();
      s1.concat(c);
    }
    storeSensor1 = s1.toInt();
    myFile.close();
  } 

  myFile = SD.open("/AutoSetup/sensor2.txt");
  if (myFile) {
    String s2;
    while (myFile.available()) {
      char c = myFile.read();
      s2.concat(c);
    }
    storeSensor2 = s2.toInt();
    myFile.close();
  } 
  pinMode(chipSelect, INPUT);
}

void AllData(){
  ssid = storeWifi.c_str();
  password = storePassword.c_str();
  webKey = storeIFTTTWebkey;
  buzzerON = storeBuzzerOn;
  zenIn1 = storeEventIn1;
  zenIn2 = storeEventIn2;
  zenIn3 = storeEventIn3;
  zenOut1 = storeEventOut1;
  zenOut2 = storeEventOut2;
  zenOut3 = storeEventOut3;
  modeNum = storeSelectedMode;
  rangeDistance = storeRange;
  sensor1 = storeSensor1;
  sensor2 = storeSensor2;
  timeToComeOn = storeTimeOn;
  timeToGoOff = storeTimeOff;
  timeMode1 = storeTimeMode1;
  timeMode2 = storeTimeMode2;
  timeMode3 = storeTimeMode3;
  gmtOffset_sec = storeGmtOffset_sec;
  daylightOffset_sec = storeDaylightOffset_sec;
}

void setRangeSD(int range){
  pinMode(chipSelect, OUTPUT);
  myFile = SD.open("/AutoSetup/range.txt", FILE_WRITE);
  if (myFile) {
    myFile.println(range);
    myFile.close();
  }
  pinMode(chipSelect, INPUT);
}

void setSelectedMode(int smode){
  pinMode(chipSelect, OUTPUT);
  myFile = SD.open("/AutoSetup/selectedMode.txt", FILE_WRITE);
  if (myFile) {
    myFile.println(smode);
    myFile.close();
  }
  pinMode(chipSelect, INPUT);
}

void setSensor1(int s1){
  pinMode(chipSelect, OUTPUT);
  myFile = SD.open("/AutoSetup/sensor1.txt", FILE_WRITE);
  if (myFile) {
    myFile.println(s1);
    myFile.close();
  }
  pinMode(chipSelect, INPUT);
}

void setSensor2(int s2){
  pinMode(chipSelect, OUTPUT);
  myFile = SD.open("/AutoSetup/sensor2.txt", FILE_WRITE);
  if (myFile) {
    myFile.println(s2);
    myFile.close();
  }
  pinMode(chipSelect, INPUT);
}

int getModeNum(){
  pinMode(chipSelect, OUTPUT);
  myFile = SD.open("/AutoSetup/selectedMode.txt");
  if (myFile) {
    String sm;
    while (myFile.available()) {
      char c = myFile.read();
      sm.concat(c);
    }
    return sm.toInt();
    myFile.close();
  } 
  pinMode(chipSelect, INPUT);
}
