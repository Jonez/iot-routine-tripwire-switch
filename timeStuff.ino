void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    displayError();
    return;
  }
  timeHour = timeinfo.tm_hour;
  timeMins = timeinfo.tm_min;
}

void keepUpdatingTime(){
  if(timeHour == 23 && timeMins == 60){
    timeHour = 00;
  }

  if(timeMins != 60){
    timeMins++;
  }

  if(timeMins == 60){
    timeMins = 00;
  }

  if(timeMins == 00){
    timeHour++;
  }

  if(timeHour > 10 && timeMins > 10){
    currentTime = String(timeHour) + ":" +  String(timeMins);
  }
  if(timeHour < 10){
    currentTime = "0" + String(timeHour) + ":" +  String(timeMins);
  }
  if(timeMins < 10){
    currentTime = String(timeHour) + ":" + "0" + String(timeMins);
  }
  if(timeHour < 10 && timeMins < 10){
    currentTime = "0" + String(timeHour) + ":" + "0" + String(timeMins);
  }
  Serial.println(currentTime);
}

void updateCurrentTime(){
  currentMillis = millis();
    if(currentMillis - startMillis >= 3600000){
      setCpuFrequencyMhz(80);
      wifiSet();
      configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
      printLocalTime();
      setCpuFrequencyMhz(20);
      startMillis = currentMillis;
    }
}

void sleepModeChange(){
  if(timeToComeOn == currentTime){
    goSleep = false;
  }
  if(timeToGoOff == currentTime){
    goSleep = true;
  }
  if(timeMode1 == currentTime){
    toggletm1 = true;
    toggletm2 = false;
    toggletm3 = false;
  }
  if(timeMode2 == currentTime){
    toggletm1 = false;
    toggletm2 = true;
    toggletm3 = false;
  }
  if(timeMode3 == currentTime){
    toggletm1 = false;
    toggletm2 = false;
    toggletm3 = true;
  }
}
