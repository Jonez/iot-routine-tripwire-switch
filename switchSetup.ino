void setupProcess(){
  setInSensorDisplay();
  delay(2000);
  setRangeDisplay();
  VL53L0X_RangingMeasurementData_t measure;
  tcaselect(0);
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
  int milimeter = measure.RangeMilliMeter;
  rangeDistance = milimeter - 100;
  setRangeSD(rangeDistance);
  delay(2000);
  setupCompleteDisplay();
  delay(2000);
  startSetup = false;
  modeNum = getModeNum();
}
