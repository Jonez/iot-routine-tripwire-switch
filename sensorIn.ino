void activeSensorIn(int modeNum, int sensorNum){
  switch (modeNum) {
    case 2:
      wifiSet();
      detected = true;
      webEvent = zenIn1;
      get_http();
      registerBuzzer();
      happyFaceDisplay();
      break;
    
    case 4:
      wifiSet();
      detected = true;
      webEvent = zenIn2;
      get_http();
      registerBuzzer();
      happyFaceDisplay();
      break;
      
    case 6:
      wifiSet();
      detected = true;
      webEvent = zenIn3;
      get_http();
      registerBuzzer();
      happyFaceDisplay();
      break;
      
    case 8:
      wifiSet();
      detected = true;
      if(toggletm1 == true){
        webEvent = zenIn1;
      }
      if(toggletm2 == true){
        webEvent = zenIn2;
      }
      if(toggletm3 == true){
        webEvent = zenIn3;
      }
      get_http();
      registerBuzzer();
      happyFaceDisplay();
      break;
      
    case 10:
      detected = true;
      registerBuzzer();
      if(sensorNum == 1){
        sensor1 = 1;
        sensor2 = 0;
        setSensor1(1);
        setSensor2(0);
      }
      if(sensorNum == 2){
        sensor1 = 0;
        sensor2 = 1;
        setSensor1(0);
        setSensor2(1);
      }
      setupProcess();
      break;
  }
}
