/*
 * Creator Matthew Jones 
 * Product Zen
 * Company Ardmo
 * DateStarted 14/04/2019
 * DateFinished 
 */
#include <Wire.h>
extern "C" { 
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}
#include <time.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define oledReset 15
#define displayHeight   64
#define displayWidth    128
Adafruit_SSD1306 display(displayWidth, displayHeight, &Wire, oledReset);

#include "Adafruit_VL53L0X.h"
Adafruit_VL53L0X lox = Adafruit_VL53L0X();


#include <HTTPClient.h>

#define PIN 26
#define buzzer 27

const char* ssid;
const char* password;

char* ntpServer = "pool.ntp.org";
long  gmtOffset_sec;
int   daylightOffset_sec;

int timeHour;
int timeMins;
String currentTime;

String webEvent;
String webKey;

unsigned long startMillis = 0;
unsigned long currentMillis;

int modeNum;
int pCount = 0;
int displayCount = 0;
int rangeDistance;
int tick = 0;
int tempCheck = 0;
unsigned long previousMillis = 0; // will store last time LED was updated
const long interval = 1000; // constants won't change

bool selectedMode = false;
bool pastSelected = false;
bool sensorIn = false;
bool sensorOut = false;
bool motion = false;
String buzzerON;
bool falseBuzz = false;
bool startSetup = false;

int sensor1;
int sensor2;

String zenIn1;
String zenIn2;
String zenIn3;

String zenOut1;
String zenOut2;
String zenOut3;

String timeToComeOn;
String timeToGoOff;
String timeMode1;
String timeMode2;
String timeMode3;
boolean toggletm1 = false;
boolean toggletm2 = false;
boolean toggletm3 = false;

boolean detected = true;
boolean goSleep = false;
boolean batteryLow = false;
