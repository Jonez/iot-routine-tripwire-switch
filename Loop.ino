void loop(){
  sleepModeChange();
  updateCurrentTime();
  
  if(goSleep == false){
    rangeRead();
  
    if(detected == true){
      setCpuFrequencyMhz(80);
      currentMillis = millis();
      if(currentMillis - startMillis >= 60000){
        keepUpdatingTime();
        batteryMonitor();
        detected = false;
        startMillis = currentMillis;
      }
    
      if(startSetup == false){
        if(tick < 5){
          displayCount++;
          if(displayCount == 45){
            displayCount = 0;
            straightFaceBlink();
          }
          if(displayCount == 1){
            straightFaceDisplay();
          }
        }
      }
    }else{
      if(startSetup == false){
        setCpuFrequencyMhz(20);
        sleepFaceDisplay();

        currentMillis = millis();
        if(currentMillis - startMillis >= 60000){
          keepUpdatingTime();
          batteryMonitor();
          startMillis = currentMillis;
        }
      }
    }
  }else{
    setCpuFrequencyMhz(10);
    tcaselect(2);
    display.clearDisplay();
    display.display();
    currentMillis = millis();
      if(currentMillis - startMillis >= 60000){
        keepUpdatingTime();
        batteryMonitor();
        startMillis = currentMillis;
        sleepModeChange();
      }
  }
}
