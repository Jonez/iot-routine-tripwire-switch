#define voltagePin 26
#define voltagePinLow 25

void batterySetup(){
  pinMode(voltagePin, INPUT);
  pinMode(voltagePinLow, OUTPUT);
}

void batteryMonitor(){
  digitalWrite(voltagePinLow, LOW);
  Serial.println(analogRead(voltagePin));

  if(analogRead(voltagePin) < 2500){
    batteryLow = true;
    batteryWarning();
  }else{
    batteryLow = false;
  }
  
  digitalWrite(voltagePinLow, HIGH);
}
